echo "Usage: <all/repo_name>"
REPO="all"
if [ "$1" != "" ]; then
  REPO="$1"
fi

# Motion Planners
if [ "$REPO" == "all" ] || [ "$REPO" == "motion_planners" ] ; then
cd motion_planners
cd cpp
mkdir build
cd build
cmake ../
make -j $(nproc)
cd ../../..
fi

# Visual SLAM
if [ "$REPO" == "all" ] || [ "$REPO" == "visual_slam" ] ; then
cd visual_slam/rtmaps/rtmaps_sdk
./build.sh
cd ../../..
fi

# Perception
if [ "$REPO" == "all" ] || [ "$REPO" == "perception" ] ; then
cd perception/caffe_dirs/SSD_CJJ
make -j $(nproc) all tools pycaffe
cd ../../..
fi

