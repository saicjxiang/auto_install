#!/bin/bash 

AD_PATH="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

# Motion Planners
MP_PATH=$AD_PATH/motion_planners
export PATH=$PATH:$MP_PATH
export DYLD_LIBRARY_PATH=$DYLD_LIBRARY_PATH:$MP_PATH
export CPLUS_INCLUDE_PATH=$CPLUS_INCLUDE_PATH:/usr/include/python2.7/
export BOOST_ROOT=/usr/local/Cellar/boost/1.64.0_1/
export BOOST_INC=/usr/local/Cellar/boost/1.64.0_1/include
export BOOST_LIB=/usr/local/Cellar/boost/1.64.0_1/lib
export BOOST_NUMPY=/usr/local/lib
export PYTHONPATH=PYTHONPATH:$MP_PATH/python:$MP_PATH/cpp/build

# Perception
CAFFE_PATH=$AD_PATH/perception/caffe_dirs/SSD_CJJ
export PYTHONPATH=$PYTHONPATH:$CAFFE_PATH/python
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:$CAFFE_PATH/build/lib
export PATH=$PATH:$CAFFE_PATH/build/tools:$CAFFE_PATH/build/scripts
export CAFFE_BIN=CAFFE_PATH/build/tools/caffe